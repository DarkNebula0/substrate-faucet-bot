require('dotenv').config();
const dbMigration = require('./db/migration');
const client = require('./modules/client');

if (!process.env.mnemonic) {
  console.error(`The environment variable 'mnemonic' was not set.`);
  process.exit();
}

if (!process.env.nodeUrl) {
  console.error(`The environment variable 'nodeUrl' was not set.`);
  process.exit();
}

if (!process.env.formats) {
  console.error(`The environment variable 'formats' was not set.`);
  process.exit();
}

if (!process.env.VERIFY_ROLE) {
  console.error(`The environment variable 'VERIFY_ROLE' was not set.`);
  process.exit();
}

dbMigration();
client();
