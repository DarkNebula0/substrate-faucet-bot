const Discord = require('discord.js');

/**
 * Creates a help Embed
 *
 * @param {import('discord.js').Client} client
 */
module.exports = (client) => {
  if (!client) {
    return null;
  }

  return new Discord.MessageEmbed()
    .setColor('#0099ff')
    .setTitle('I hope that I can help you with this...')
    .addField('Show help', '```!fc help```')
    .addField('---', '_ _')
    .addField('Get tokens to play and test', '```!fc get <Address>```')
    .addFields({
      name: 'Example',
      value: '!fc get 3RrwvaEF5zXb26Fz9rcQpDWS57CtERHpNehXCPcNoHGKutQY',
    })
    .addField(
      'Get subkey infromation for Address',
      '```!fc subkey <Address>```',
    )
    .addFields({
      name: 'Example',
      value: '!fc subkey 3RrwvaEF5zXb26Fz9rcQpDWS57CtERHpNehXCPcNoHGKutQY',
    })
    .addField('Verify your account', '```!fc verify <Address>```')
    .addFields({
      name: 'Example',
      value: '!fc subkey 3RrwvaEF5zXb26Fz9rcQpDWS57CtERHpNehXCPcNoHGKutQY',
    })
    .addField('---', '_ _')
    .setTimestamp()
    .setImage(client.user.avatarURL())
    .setFooter(client.user.username);
};
