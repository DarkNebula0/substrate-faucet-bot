const createHelpEmbed = require('../embeds/helpEmbed');
const faucetHandler = require('./faucetHandler');
const subkeyHandler = require('./subkeyHandler');
const verifyHandler = require('./verifyHandler');

/**
 * Chat messages are processed here
 *
 * @param {import('discord.js').Message} msg
 */
module.exports = async (msg) => {
  // The message object is invalid, comes from a bot or is not destined for the bot
  if (!msg || msg.author.bot || !msg.content.startsWith('!fc')) {
    return;
  }

  const msgArgs = msg.content.split(/\s/);

  switch (msgArgs[1]) {
    case 'get': {
      return faucetHandler(msg, msgArgs.slice(2));
    }
    case 'subkey': {
      return subkeyHandler(msg, msgArgs.slice(2));
    }
    case 'verify': {
      return verifyHandler(msg, msgArgs.slice(2));
    }
    default:
    case 'help': {
      const embed = createHelpEmbed(msg.client);
      return embed && msg.channel.send(embed);
    }
  }
};
