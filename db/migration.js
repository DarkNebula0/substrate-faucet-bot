const { default: migrate } = require('node-pg-migrate');

const { User, Password, Database, Host, Port } = process.env;
module.exports = async () => {
  const options = {
    databaseUrl: {
      host: Host,
      port: Port,
      user: User,
      password: Password,
      database: Database,
    },
    migrationsTable: 'pgmigrations',
    dir: 'migrations',
    direction: 'up',
    count: Infinity,
  };

  try {
    await migrate(options);
  } catch (e) {
    console.error('DB migration has failed', e);
    process.exit(1);
  }
};
