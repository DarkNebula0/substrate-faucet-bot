const { Pool } = require('pg');

const { User, Password, Database, Host, Port } = process.env;
const pool = new Pool({
  host: Host,
  port: Port,
  user: User,
  password: Password,
  database: Database,
});

pool.on('error', (err) => {
  console.error('Unexpected error on idle client', err);
  process.exit(-1);
});

module.exports = {
  query: (text, params, callback) => {
    return pool.query(text, params, callback);
  },
};
