FROM parity/subkey
USER root

RUN apt-get update
RUN apt-get -y install curl gnupg
RUN curl -sL https://deb.nodesource.com/setup_15.x  | bash -
RUN apt-get -y install nodejs

WORKDIR /app

COPY ./ ./
RUN npm install

ENTRYPOINT []
CMD nodejs index.js