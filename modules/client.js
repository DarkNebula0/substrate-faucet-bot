const Discord = require('discord.js');
const registerClientEvents = require('../events/clientEvents');

module.exports = async () => {
  global.client = new Discord.Client();

  if (!process.env.token) {
    console.error(`The environment variable 'token' was not set `);
    process.exit();
  }

  if (!(await registerClientEvents(client))) {
    console.error('Discord client events could not be registered');
    process.exit();
  }

  client.login(process.env.token);
};
